package pl.econsulting.model;

import java.io.Serializable;
import java.util.List;

public class Employ implements Serializable {

	private static final long serialVersionUID = -4055769626856828082L;
	
	private String name = null;
	private String surname = null;
	private String email = null;
	private String username = null;
	private String password = null;
	private String userRole = null;
	private List<String> addedToLists = null;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<String> getAddedToLists() {
		return addedToLists;
	}
	public void setAddedToLists(List<String> addedToLists) {
		this.addedToLists = addedToLists;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
}
