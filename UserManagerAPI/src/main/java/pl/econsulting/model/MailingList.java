package pl.econsulting.model;

import java.util.List;

public class MailingList {
	
	private String name;
	private String mailAddress;
	private List<String> addressList;
	private List<String> availavleAddresses;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getMailAddress() {
		return mailAddress;
	}
	
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}
	
	public List<String> getAddressList() {
		return addressList;
	}
	
	public void setAddressList(List<String> addressList) {
		this.addressList = addressList;
	}

	public List<String> getAvailavleAddresses() {
		return availavleAddresses;
	}

	public void setAvailavleAddresses(List<String> availavleAddresses) {
		this.availavleAddresses = availavleAddresses;
	}
}
