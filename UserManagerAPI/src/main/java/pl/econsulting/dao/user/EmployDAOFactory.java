package pl.econsulting.dao.user;

public class EmployDAOFactory {
	
	private static EmployDAO employDAO;
	
	private EmployDAOFactory() {}
	
	public static final synchronized EmployDAO getEmployDAO() {
		if (employDAO == null) {
			employDAO = new LDAPEmployDAO();
			return employDAO;
		} else
			return employDAO;
	}
}
