package pl.econsulting.dao.user;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InvalidAttributeValueException;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.sql.DataSource;

import pl.econsulting.common.MySQLConnectionPoolFactory;
import pl.econsulting.dao.DAOException;
import pl.econsulting.common.LDAPConnectionFactory;
import pl.econsulting.model.Employ;

public class LDAPEmployDAO implements EmployDAO {
	
	final static private int UF_ACCOUNTDISABLE = 0x0002;
	private static final int UF_NORMAL_ACCOUNT = 0x0200;
	
    private static final String DOMAIN_NAME = "ec";
    private static final String DOMAIN_ROOT = "DC=ec,DC=lan";
    
    private static final String FIND_ALL_MAILINGLISTS_FOR_USER_QUERY = 
			"select name from alias where goto like ?";

	@Override
	public void createEmploy(Employ user) {
		try {
			if (checkIfEmilWasAdded(user.getEmail()))
				throw new DAOException("This email has already been used!");
		} catch (NamingException e) {
			e.printStackTrace();
			throw new DAOException("unable to check if a given email has already been added");
		}
		try {
			InitialLdapContext ctx  = LDAPConnectionFactory.getLDAPConnection();
			if ((user.getUsername() == null) || (user.getPassword() == null) || (user.getName() == null)
					|| (user.getSurname() == null) || (user.getEmail() == null) || (user.getUserRole() == null))
				throw new DAOException("not all attributes were given!");
			Attributes container = new BasicAttributes();
			
			// Create the objectclass to add
	        Attribute objClasses = new BasicAttribute("objectClass");
	        objClasses.add("top");
	        objClasses.add("person");
	        objClasses.add("organizationalPerson");
	        objClasses.add("user");
	        
	        String cnString = user.getName()+" "+user.getSurname();
	        Attribute cn = new BasicAttribute("cn",cnString);
	        Attribute sAMAccountName = new BasicAttribute("sAMAccountName", user.getUsername());
	        Attribute principalName = new BasicAttribute("userPrincipalName", user.getUsername()+"@"+DOMAIN_NAME);
	        Attribute givenName = new BasicAttribute("givenName", user.getName());
	        Attribute mail = new BasicAttribute("mail", user.getEmail());
	        Attribute sn = new BasicAttribute("sn", user.getSurname());
	        Attribute uid = new BasicAttribute("uid", user.getUsername());
	        
	        // Add password
	        //Attribute userPassword = new BasicAttribute("userpassword", password);
	        String newQuotedPassword = "\"" + user.getPassword() + "\"";
	        byte[] newUnicodePassword = null;
			newUnicodePassword = newQuotedPassword.getBytes("UTF-16LE");
			Attribute unicodePwd = new BasicAttribute("unicodePwd", newUnicodePassword);
			Attribute userCont = new BasicAttribute("userAccountControl", Integer.toString(UF_NORMAL_ACCOUNT));
			
			//add attributes to the container
			container.put(objClasses);
	        container.put(sAMAccountName);
	        container.put(principalName);
	        container.put(cn);
	        container.put(sn);
	        container.put(mail);
	        container.put(givenName);
	        container.put(uid);
	        container.put(unicodePwd);
	        container.put(userCont);
	        
	        ctx.createSubcontext("CN="+cnString+",OU="+user.getUserRole()+",OU=_root"+","+DOMAIN_ROOT, container);
	        
		} catch (InvalidAttributeValueException e) {
			if (e.getMessage().contains("0000052D"))
				throw new DAOException("Password is too short");
			else
				throw new DAOException(e.getMessage());
		} catch (NameAlreadyBoundException e) {
			throw new DAOException("This user already exists!");
		} catch (NamingException  e) {
			e.printStackTrace();
			throw new DAOException("LDAP connection has failed");
		} catch (UnsupportedEncodingException e) {
			throw new DAOException("unable to compute user's password!");
		}
	}

	@Override
	public Employ readEmploy(String username) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Employ toReturn = new Employ();
		try {
			InitialLdapContext ctx  = LDAPConnectionFactory.getLDAPConnection();
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			searchControls.setTimeLimit(30000);
			NamingEnumeration<SearchResult> sr = ctx.search("OU=_root,DC=ec,DC=lan", "sAMAccountName="+username, searchControls);
			if (!sr.hasMoreElements())
				throw new DAOException("there's no such an user!");
			while (sr.hasMore()) {
				SearchResult next = sr.next();
				Attributes attrs = next.getAttributes();
				toReturn.setName(attrs.get("givenName").toString().replace("givenName:", "").trim());
				toReturn.setSurname(attrs.get("sn").toString().replace("sn:", "").trim());
				toReturn.setUsername(attrs.get("sAMAccountName").toString().replace("sAMAccountName:", "").trim());
				if (attrs.get("mail") == null)
					toReturn.setEmail("");
				else
					toReturn.setEmail(attrs.get("mail").toString().replace("mail:", "").trim());
				DataSource ds = MySQLConnectionPoolFactory.getDataSource();
				con = ds.getConnection();
				if (!toReturn.getEmail().isEmpty()) {
					pstmt = con.prepareStatement(FIND_ALL_MAILINGLISTS_FOR_USER_QUERY);
					pstmt.setString(1, "%"+toReturn.getEmail()+"%");
					rs = pstmt.executeQuery();
					ArrayList<String> addresses = new ArrayList<>();
					while (rs.next())
						addresses.add(rs.getString(1));
					toReturn.setAddedToLists(addresses);
				}
			}
		} catch (NamingException e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		} catch (SQLException e) {
			
		} finally { 
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {}
			}
		}
		return toReturn;
	}

	@Override
	public void updateEmploy(Employ user) throws DAOException {
		if ((user.getName() == null) || (user.getSurname() == null) || (user.getEmail() == null))
			throw new DAOException("incomplete parameters list");
		/*try {
			if (checkIfEmilWasAdded(user.getEmail()))
				throw new DAOException("This email has already been used!");
		} catch (NamingException e) {
			e.printStackTrace();
			throw new DAOException("unable to check if a given email has already been added");
		}*/
		try {
			InitialLdapContext ctx  = LDAPConnectionFactory.getLDAPConnection();
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			searchControls.setTimeLimit(30000);
			NamingEnumeration<SearchResult> sr = ctx.search("OU=_root,DC=ec,DC=lan", "sAMAccountName="+user.getUsername(), searchControls);
			//this loop should execute only once
			while (sr.hasMore()) {
				SearchResult next = sr.next();
				String userDN = next.getNameInNamespace();
				ModificationItem[] mods = new ModificationItem[3];
				mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("mail", user.getEmail()));
				mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("givenName", user.getName()));
				mods[2] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("sn", user.getSurname()));
				ctx.modifyAttributes(userDN, mods);
				ctx.close();
			}
		} catch (NamingException e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		}
	}

	@Override
	public void deleteEmploy(Employ user) throws DAOException {
		//this method actually doesn't delete user's account, it disables it and moves to "deleted" OU
		if (user.getUsername() == null)
			throw new DAOException("username cannot be null!");
		try {
			this.readEmploy(user.getUsername());
			InitialLdapContext ctx  = LDAPConnectionFactory.getLDAPConnection();
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			searchControls.setTimeLimit(30000);
			NamingEnumeration<SearchResult> sr = ctx.search("OU=_root,DC=ec,DC=lan", "sAMAccountName="+user.getUsername(), searchControls);
			//this loop should execute only once
			while (sr.hasMore()) {
				SearchResult next = sr.next();
				
				//this moves user's account to deleted OU
				String oldUserDN = next.getNameInNamespace();
				String newUserDN = oldUserDN.replace("OU=Employees", "OU=!Disabled").replace("OU=Contractors", "OU=!Disabled");
				ctx.rename(oldUserDN, newUserDN);
				
				//this disables user's account
				int accountCtrl = Integer.parseInt(ctx.getAttributes(newUserDN).get("userAccountControl")
						.toString().replace("userAccountControl:","").trim());
				ModificationItem[] mods = new ModificationItem[1];
				mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("userAccountControl", 
						Integer.toString(accountCtrl + UF_ACCOUNTDISABLE)));
				ctx.modifyAttributes(newUserDN, mods);
				ctx.close();
			}
		} catch (NamingException e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		}
	}

	@Override
	public List<Employ> findAll() {
		List<Employ> allEmployees = new ArrayList<Employ>();
		try {
			InitialLdapContext ctx  = LDAPConnectionFactory.getLDAPConnection();
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			searchControls.setTimeLimit(30000);
			NamingEnumeration<SearchResult> sr = ctx.search("OU=_root,DC=ec,DC=lan", "(objectclass=user)", searchControls);
			while (sr.hasMore()) {
				SearchResult next = sr.next();
				if ((next.getNameInNamespace().contains("OU=Employees")) || (next.getNameInNamespace().contains("OU=Contractors"))) {
					Attributes attrs = next.getAttributes();
					Employ tempEmploy = new Employ();
					tempEmploy.setName(attrs.get("givenName").toString().replace("givenName:", "").trim());
					tempEmploy.setSurname(attrs.get("sn").toString().replace("sn:", "").trim());
					tempEmploy.setUsername(attrs.get("sAMAccountName").toString().replace("sAMAccountName:", "").trim());
					if (attrs.get("mail") == null)
						tempEmploy.setEmail("");
					else
						tempEmploy.setEmail(attrs.get("mail").toString().replace("mail:", "").trim());
					if (attrs.get("distinguishedName").contains("OU=Employees"))
						tempEmploy.setUserRole("employ");
					else if (attrs.get("distinguishedName").contains("OU=Contractors"))
						tempEmploy.setUserRole("contractor");
					allEmployees.add(tempEmploy);
				}
			}
			ctx.close();
		} catch (NamingException e) {
			e.printStackTrace();
			throw new DAOException("LDAP connection has failed");
		}
		return allEmployees;
	}

	@Override
	public Employ readEmploy(int employId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private boolean checkIfEmilWasAdded(String email) throws NamingException {
		InitialLdapContext ctx = LDAPConnectionFactory.getLDAPConnection();
		SearchControls sc = new SearchControls();
		sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
		sc.setTimeLimit(30000);
		NamingEnumeration<SearchResult> sr = ctx.search("OU=_root,DC=ec,DC=lan", "mail="+email, sc);
		ctx.close();
		if (sr.hasMoreElements())
			return true;
		else
			return false;
	}
}
