/*
 * The Employ class has been modified since MySQLEmployDAO has been used for the last time.
 * Attributes username, password and jobRole have been added.
 * Before reusing MySQLEmployDAO, amend it first! 
 */
/*
package pl.econsulting.dao.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import pl.econsulting.common.MySQLConnectionPoolFactory;
import pl.econsulting.dao.DAOException;
import pl.econsulting.model.Employ;

public class MySQLEmployDAO implements EmployDAO{

	private static final String CREATE_USER_QUERY = 
			"insert into employees(Name, Surname, email) values(?,?,?)";
	private static final String SELECT_EMPLOY_QUERY_EMAIL = 
			"select Name, Surname, email, Id from employees where email = ?";
	private static final String SELECT_EMPLOY_QUERY_ID = 
			"select Name, Surname, email, Id from employees where id = ?";
	private static final String UPDATE_USER_QUERY = 
			"updare employees set Name=? Surname=? email=? where email=?";
	private static final String DELETE_USER_QUERY =
			"delete from employees where Id=?";
	private static final String FIND_ALL_USERS_QUERY =
			"select Name, Surname, email, Id from employees";
	private static final String FIND_ALL_MAILINGLISTS_FOR_USER_QUERY = 
			"select name from alias where goto like ?";
	
	@Override
	public void createEmploy(Employ user) throws DAOException {
		DataSource ds = MySQLConnectionPoolFactory.getDataSource();
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareCall(CREATE_USER_QUERY)) {
			pstmt.setString(1, user.getName());
			pstmt.setString(2, user.getSurname());
			pstmt.setString(3, user.getEmail());
			pstmt.execute();
		} catch (MySQLIntegrityConstraintViolationException e) {
			throw new DAOException("Duplicatd entry!");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException("Database error");
			//TODO: add logging 
		}
	}

	@Override
	public Employ readEmploy(String email) throws DAOException {
		Employ user=null;
		DataSource ds = MySQLConnectionPoolFactory.getDataSource();
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(SELECT_EMPLOY_QUERY_EMAIL)) {
			pstmt.setString(1, email);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				user = new Employ();
				user.setName(rs.getString(1));
				user.setSurname(rs.getString(2));
				user.setEmail(rs.getString(3));
				user.setEmployId(rs.getInt(4));
				PreparedStatement pstmt2 = con.prepareStatement(FIND_ALL_MAILINGLISTS_FOR_USER_QUERY);
				pstmt2.setString(1, "%"+user.getEmail()+"%");
				ResultSet rs2 = pstmt2.executeQuery();
				ArrayList<String> addresses = new ArrayList<>();
				while (rs2.next())
					addresses.add(rs2.getString(1));
				user.setAddedToLists(addresses);
				rs2.close();
				pstmt2.close();
				if (rs.next()) 
					throw new DAOException("There's morre than one record");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO: add logging 
		}
		return user;
	}

	@Override
	public void updateEmploy(Employ user) throws DAOException {
		DataSource ds = MySQLConnectionPoolFactory.getDataSource();
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(UPDATE_USER_QUERY)) {
			pstmt.setString(1, user.getName());
			pstmt.setString(2, user.getSurname());
			pstmt.setString(3, user.getEmail());
			pstmt.setString(4, user.getEmail());
			pstmt.execute();
		} catch (SQLException e) {
			//TODO: add logging
			throw new DAOException(e);
		}
	}

	@Override
	public void deleteEmploy(Employ user) {
		DataSource ds = MySQLConnectionPoolFactory.getDataSource();
		try (Connection con = ds.getConnection(); 
				PreparedStatement pstmt = con.prepareStatement(DELETE_USER_QUERY)) {
			this.readEmploy(user.getEmployId());
			pstmt.setInt(1, user.getEmployId());
			pstmt.execute();
		} catch (SQLException e) {
			//TODO: add logging
			throw new DAOException(e);
		}
	}

	@Override
	public List<Employ> findAll() {
		List<Employ> allEmployees= new ArrayList<>();
		DataSource ds = MySQLConnectionPoolFactory.getDataSource();
		try (Connection con = ds.getConnection(); 
				PreparedStatement pstmt = con.prepareStatement(FIND_ALL_USERS_QUERY)) {
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Employ tmpUser = new Employ();
				tmpUser.setName(rs.getString(1));
				tmpUser.setSurname(rs.getString(2));
				tmpUser.setEmail(rs.getString(3));
				tmpUser.setEmployId(rs.getInt(4));
				PreparedStatement pstmt2 = con.prepareStatement(FIND_ALL_MAILINGLISTS_FOR_USER_QUERY);
				pstmt2.setString(1, "%"+tmpUser.getEmail()+"%");
				ResultSet rs2 = pstmt2.executeQuery();
				ArrayList<String> addresses = new ArrayList<>();
				while (rs2.next())
					addresses.add(rs2.getString(1));
				tmpUser.setAddedToLists(addresses);
				rs2.close();
				pstmt2.close();
				allEmployees.add(tmpUser);
			}
		} catch (SQLException e) {
			//TODO: add logging
			throw new DAOException(e);
		}
		return allEmployees;
	}

	@Override
	public Employ readEmploy(int employId) {
		Employ user=null;
		DataSource ds = MySQLConnectionPoolFactory.getDataSource();
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(SELECT_EMPLOY_QUERY_ID)) {
			pstmt.setInt(1, employId);
			ResultSet rs = pstmt.executeQuery();
			if (!rs.isBeforeFirst())
				throw new DAOException("there's no such an user");
			while (rs.next()) {
				user = new Employ();
				user.setName(rs.getString(1));
				user.setSurname(rs.getString(2));
				user.setEmail(rs.getString(3));
				user.setEmployId(rs.getInt(4));
				PreparedStatement pstmt2 = con.prepareStatement(FIND_ALL_MAILINGLISTS_FOR_USER_QUERY);
				pstmt2.setString(1, "%"+user.getEmail()+"%");
				ResultSet rs2 = pstmt2.executeQuery();
				ArrayList<String> addresses = new ArrayList<>();
				while (rs2.next())
					addresses.add(rs2.getString(1));
				user.setAddedToLists(addresses);
				rs2.close();
				pstmt2.close();
				if (rs.next()) 
					throw new DAOException("There's morre than one record");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO: add logging 
		}
		return user;
	}
}
*/