package pl.econsulting.dao.user;

import java.util.List;

import pl.econsulting.model.Employ;

public interface EmployDAO {
	
	public void createEmploy(Employ user);
	
	public Employ readEmploy(String email);
	
	public void updateEmploy(Employ user);
	
	public void deleteEmploy(Employ user);
	
	public List<Employ> findAll();
	
	public Employ readEmploy(int employId);
}
