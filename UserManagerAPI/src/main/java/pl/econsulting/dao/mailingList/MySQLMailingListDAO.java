package pl.econsulting.dao.mailingList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.sql.DataSource;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import pl.econsulting.common.MySQLConnectionPoolFactory;
import pl.econsulting.dao.DAOException;
import pl.econsulting.common.LDAPConnectionFactory;
import pl.econsulting.model.MailingList;

public class MySQLMailingListDAO implements MailingListDAO {
	
	private static final String GET_MAILINGLIST_BY_NAME = 
			"select address, goto, name from alias where name=?;";
	private static final String GET_MAILINGLIST_BY_ADDRESS = 
			"select address, goto, name from alias where address=?;";
	private static final String GET_ALL_MAILLISTS = 
			"select address, goto, name from alias where name !='';";
	private static final String UPDATE_MAILING_LIST =
			"update alias set goto=?, name=? where address=?;";
	private static final String GET_GOTO_QUERY = 
			"select goto from alias where name=?;";
	private static final String ADD_MAILINGLIST_WITH_NAME = 
			"insert into alias(address, name, goto) values(?,?,?)";
	private static final String ADD_MAILINGLIST_WIHOUT_NAME =
			"insert into alias(address, goto) values(?,?)";
	private static final String DELETE_MAILINGLIST = 
			"delete from alias where address=?";
	
	@Override
	public boolean createMailingList(MailingList list)  throws DAOException {
		DataSource ds = MySQLConnectionPoolFactory.getDataSource();
		String listAddress = list.getMailAddress();
		List<String> gotoList = list.getAddressList();
		String name = list.getName();
		if ((listAddress == null) || (name == null) || (gotoList == null))
			throw new DAOException("not all attributes were given!");
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(ADD_MAILINGLIST_WITH_NAME);
				PreparedStatement pstmt2 = con.prepareStatement(ADD_MAILINGLIST_WIHOUT_NAME);) {
			
			//this adds a regular mailing list (with name)
			pstmt.setString(1, listAddress);
			pstmt.setString(2, name);
			StringBuffer sb = new StringBuffer();
			for (String singleEmail : gotoList)
				sb = sb.append(singleEmail+",");
			pstmt.setString(3, sb.toString().substring(0, sb.length()-1));
			pstmt.execute();
			
			//this adds a legacy mailing list (without name and in lists.econslting.pl domain)
			pstmt2.setString(1, listAddress.replaceAll("@.*", "-l@lists.econsulting.pl"));
			pstmt2.setString(2,listAddress);
			pstmt2.execute();
			return true;
		} catch (MySQLIntegrityConstraintViolationException e) {
			throw new DAOException("Such mailing list already exists!");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		}
	}

	/**
	 * reads mailinglist by its name.
	 * 
	 * @author kzadmin
	 * @param name the name of a mailinglist
	 * @return a found mailinglist
	 * @throws DAOException if there's more than one list with a given name, or if there isn't such a list  
	 */
	@Override
	public MailingList readMailingList(String name) {
		DataSource ds = MySQLConnectionPoolFactory.getDataSource();
		MailingList list = null;
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(GET_MAILINGLIST_BY_NAME);) {
			pstmt.setString(1, name);
			ResultSet rs = pstmt.executeQuery();
			if (!rs.isBeforeFirst())
				throw new DAOException("there's no such a mailinglist!");
			while (rs.next()) {
				list = new MailingList();
				list.setName(rs.getString("name"));
				list.setMailAddress(rs.getString("address"));
				ArrayList<String> emailAddresses = new ArrayList<>();
				if (rs.getString("goto") != null) {
					String[] emailAddressesString = rs.getString("goto").split(",");
					for (String line: emailAddressesString) {
						emailAddresses.add(line);
					}
				}
				list.setAddressList(emailAddresses);
				list.setAvailavleAddresses(getAvailableAddressList(list.getName()));
				if (rs.next())
					throw new DAOException("there's more than one list with this name!");
			}
			if (rs != null)
				rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public boolean deleteMailingList(MailingList list) throws DAOException {
		DataSource ds = MySQLConnectionPoolFactory.getDataSource();
		if (list.getMailAddress() == null)
			throw new DAOException("list's address cannot be null");
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(GET_MAILINGLIST_BY_NAME);) {
			pstmt.setString(1, list.getMailAddress());
			ResultSet rs = pstmt.executeQuery();
			if (rs.isBeforeFirst())
				throw new DAOException("there's no such a list in a database");
			//removes mailing list in econsulting.com.pl domain
			PreparedStatement pstmt2 = con.prepareStatement(DELETE_MAILINGLIST);
			pstmt2.setString(1, list.getMailAddress());
			pstmt2.execute();
			//pstmt2.close();
			
			//removes a legacy mailing list in list.econsulting.pl domain (if there's any)
			pstmt2 = con.prepareStatement(GET_MAILINGLIST_BY_ADDRESS);
			pstmt2.setString(1, list.getMailAddress().replaceAll("@.*", "-l@lists.econsulting.pl"));
			rs = pstmt2.executeQuery();
			if (rs.isBeforeFirst()) {
				pstmt2 = con.prepareStatement(DELETE_MAILINGLIST);
				pstmt2.setString(1, list.getMailAddress().replaceAll("@.*", "-l@lists.econsulting.pl"));
				pstmt2.execute();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		}
		return false;
	}

	@Override
	public boolean updateMailingList(MailingList list) {
		DataSource ds = MySQLConnectionPoolFactory.getDataSource();
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(UPDATE_MAILING_LIST);) {
			if ((list.getName() == null) || (list.getAddressList() == null) || (list.getMailAddress() == null))
				throw new DAOException("List name, goto or address list is empty!");
			else {
				pstmt.setString(2, list.getName());
				StringBuffer sb = new StringBuffer();
				Iterator<String> iter = list.getAddressList().iterator();
				while (iter.hasNext()){
					String tmp = (String)iter.next();
					if (iter.hasNext())
						sb.append(tmp+",");
					else
						sb.append(tmp);
				}
				pstmt.setString(1, sb.toString());
				pstmt.setString(3, list.getMailAddress());
				pstmt.execute();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		}
		return true;
	}
	
	@Override
	public List<MailingList> readAllMailingLists() {
		DataSource ds = MySQLConnectionPoolFactory.getDataSource();
		ArrayList<MailingList> list = new ArrayList<MailingList>();
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(GET_ALL_MAILLISTS);) {
			ResultSet rs = pstmt.executeQuery();
			if (!rs.isBeforeFirst())
				throw new DAOException("there aren't any mailing lists!");
			while (rs.next()) {
				MailingList temp = new MailingList();
				temp.setName(rs.getString("name"));
				temp.setMailAddress(rs.getString("address"));
				ArrayList<String> emailAddresses = new ArrayList<>();
				if (rs.getString("goto") != null) {
					String[] emailAddressesString = rs.getString("goto").split(",");
					for (String line: emailAddressesString) {
						emailAddresses.add(line);
					}
				}
				temp.setAddressList(emailAddresses);
				temp.setAvailavleAddresses(getAvailableAddressList(temp.getName()));
				list.add(temp);
			}
			if (rs != null)
				rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * Returns all available email addresses which haven't been added yet to a given mailinglist.
	 * 
	 * @param listName
	 * @return
	 * @throws DAOException
	 */
	private ArrayList<String> getAvailableAddressList(String listName) throws DAOException {
		ArrayList<String> toReturn = new ArrayList<>();
		DataSource ds = MySQLConnectionPoolFactory.getDataSource();
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(GET_GOTO_QUERY);) {
			pstmt.setString(1, listName);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				String gotoList = "";
				if (rs.getString(1) != null)
					gotoList = rs.getString(1);
				if (rs.next())
					throw new DAOException("there's more than one list with this name!");
				List<String> gototable = Arrays.asList(gotoList.replaceAll("\\s+","").toLowerCase().split(","));
				InitialLdapContext ctx  = LDAPConnectionFactory.getLDAPConnection();
				SearchControls searchControls = new SearchControls();
				searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
				searchControls.setTimeLimit(30000);
				NamingEnumeration<SearchResult> sr = ctx.search("OU=_root,DC=ec,DC=lan", "(&(mail=*)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))", searchControls);
				while (sr.hasMore()) {
					SearchResult next = sr.next();
					Attributes attrs = next.getAttributes();
					if (!gototable.contains(attrs.get("mail").toString().replace("mail:", "").trim().toLowerCase()) 
							&& (!toReturn.contains(attrs.get("mail").toString().replace("mail:", "").trim())))
						toReturn.add(attrs.get("mail").toString().replace("mail:", "").trim());
				}
				ctx.close();
			}
		} catch (SQLException  e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		} catch (NamingException e) {
			e.printStackTrace();
			throw new DAOException(e.getMessage());
		}
		return toReturn;
	}

	@Override
	public List<String> readAllEmailAddresses() {
		InitialLdapContext ctx;
		try {
			ctx  = LDAPConnectionFactory.getLDAPConnection();
		} catch (NamingException e) {
			throw new DAOException("unable to connect to LDAP!");
		}
		try {
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			searchControls.setTimeLimit(30000);
			NamingEnumeration<SearchResult> sr = ctx.search("OU=_root,DC=ec,DC=lan", "mail=*", searchControls);
			List<String> toReturn = new ArrayList<>();
			while (sr.hasMore()) {
				SearchResult next = sr.next();
				Attributes attrs = next.getAttributes();
				toReturn.add(attrs.get("mail").toString().replaceAll("mail:", "").trim());
			}
			return toReturn;
		} catch (NamingException e) {
			throw new DAOException("unable to query for users!");
		}
	}

	@Override
	public MailingList readMailingListByAddress(String emailAddress) {
		DataSource ds = MySQLConnectionPoolFactory.getDataSource();
		MailingList list = null;
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(GET_MAILINGLIST_BY_ADDRESS);) {
			pstmt.setString(1, emailAddress);
			ResultSet rs = pstmt.executeQuery();
			if (!rs.isBeforeFirst())
				throw new DAOException("there's no such a mailinglist!");
			while (rs.next()) {
				list = new MailingList();
				list.setName(rs.getString("name"));
				list.setMailAddress(rs.getString("address"));
				ArrayList<String> emailAddresses = new ArrayList<>();
				if (rs.getString("goto") != null) {
					String[] emailAddressesString = rs.getString("goto").split(",");
					for (String line: emailAddressesString) {
						emailAddresses.add(line);
					}
				}
				list.setAddressList(emailAddresses);
				list.setAvailavleAddresses(getAvailableAddressList(list.getName()));
				if (rs.next())
					throw new DAOException("there's more than one list with this name!");
			}
			if (rs != null)
				rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
}