package pl.econsulting.dao.mailingList;

public class MailingListDAOFactory {

	private static MailingListDAO mailingListDAO;
	
	private MailingListDAOFactory() {}
	
	public static final synchronized MailingListDAO getMailingListDAOFactory() {
		if (mailingListDAO == null) {
			mailingListDAO = new MySQLMailingListDAO();
			return mailingListDAO;
		} else
			return mailingListDAO;
	}
}
