package pl.econsulting.dao.mailingList;

import java.util.List;
import pl.econsulting.model.MailingList;

public interface MailingListDAO {
	
	public boolean createMailingList(MailingList list);
	
	public MailingList readMailingList(String name);
	
	public MailingList readMailingListByAddress(String emailAddress);
	
	public boolean deleteMailingList(MailingList list);
	
	public boolean updateMailingList(MailingList list);
	
	public List<MailingList> readAllMailingLists();
	
	public List<String> readAllEmailAddresses();
}