package pl.econsulting.dao;

public class DAOException extends RuntimeException {
	
	private static final long serialVersionUID = 8981675242785449258L;

	public DAOException(String message) {
		super(message);
	}
	
	public DAOException(Throwable reason) {
		super(reason);
	}
	
	public DAOException(Throwable reason, String message) {
		super(message, reason);
	}
}
