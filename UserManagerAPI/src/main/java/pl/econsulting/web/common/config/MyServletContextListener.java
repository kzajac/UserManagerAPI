package pl.econsulting.web.common.config;

import java.io.*;
import java.util.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class MyServletContextListener implements ServletContextListener {
	
	private static final String filename = "/WEB-INF/configuration.properties";
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("contextDestroyed");
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		String line = "";
		Hashtable<String, String> prop = new Hashtable<String, String>();
		ServletContext context = arg0.getServletContext();
		InputStream is = context.getResourceAsStream(filename);
		if (is != null) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			try {
				while ((line = reader.readLine()) != null)
					prop.put(line.split("==")[0], line.split("==")[1]);
				AppProperties.setProp(prop);
				if (!prop.containsKey("INITIAL_CONTEXT_FACTORY") || 
						!prop.containsKey("SECURITY_AUTHENTICATION") ||
						!prop.containsKey("PROVIDER_URL") ||
						!prop.containsKey("SECURITY_PRINCIPAL_DOMAIN") ||
						!prop.containsKey("SEARCH_BASE") ||
						!prop.containsKey("MEMBER_OF") ||
						!prop.containsKey("ADMIN_NAME") ||
						!prop.containsKey("ADMIN_PASS") ||
						!prop.containsKey("TOKEN_TTL") ||
						!prop.containsKey("TOKEN_PASSWORD") ||
						!prop.containsKey("SEARCH_BASE") ||
						!prop.containsKey("MEMBER_OF") 
						)
					throw new RuntimeException("no all needed properties were set");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
