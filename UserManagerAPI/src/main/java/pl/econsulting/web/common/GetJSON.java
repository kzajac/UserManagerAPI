package pl.econsulting.web.common;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;

public class GetJSON {
	
	public static JSONObject getJson(HttpServletRequest request) throws IOException {
		StringBuffer sb = new StringBuffer();
		BufferedReader br = request.getReader();
		String line;
		while ((line = br.readLine())!=null) 
			sb.append(line);
		JSONObject json = new JSONObject(sb.toString());
		return json;
	}
}
