package pl.econsulting.web.common.config;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

public class AppProperties extends Properties{
	
	private static final long serialVersionUID = -6116716049446033638L;
	private static Hashtable<String, String> prop;
	private static AppProperties instance;
	
	private AppProperties() {}
	
	public static final AppProperties getAppProperties() {
		if (instance == null) {
			if (prop != null) {
				instance = new AppProperties();
				Enumeration<String> keys = prop.keys();
				while(keys.hasMoreElements()) {
					String key = keys.nextElement();
					String value = prop.get(key);
					instance.put(key, value);
				}
			} else
				return null;
		} 
		return instance;
	}

	public static Hashtable<String, String> getProp() {
		return prop;
	}
	public static void setProp(Hashtable<String, String> prop) {
		AppProperties.prop = prop;
	}	
}
