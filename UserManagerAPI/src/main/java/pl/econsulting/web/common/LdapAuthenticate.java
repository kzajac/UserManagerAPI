package pl.econsulting.web.common;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.security.auth.login.FailedLoginException;

import pl.econsulting.web.common.config.AppProperties;

import javax.naming.directory.Attribute;

public class LdapAuthenticate {
	
	public static boolean authenticate(String username, String password) throws FailedLoginException {
		Hashtable<String, String> env = new Hashtable<String, String>();
		AppProperties prop = AppProperties.getAppProperties();
		
		//set auth method, username and pass
		env.put(Context.INITIAL_CONTEXT_FACTORY, prop.getProperty("INITIAL_CONTEXT_FACTORY"));
		env.put(Context.SECURITY_AUTHENTICATION, prop.getProperty("SECURITY_AUTHENTICATION"));
		env.put(Context.SECURITY_PRINCIPAL, username+prop.getProperty("SECURITY_PRINCIPAL_DOMAIN"));
		env.put(Context.SECURITY_CREDENTIALS, password);
		env.put(Context.PROVIDER_URL, prop.getProperty("PROVIDER_URL"));
		
		try {
			LdapContext ctx = new InitialLdapContext(env, null);
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			searchControls.setTimeLimit(30000);
			NamingEnumeration<SearchResult> sr = ctx.search(prop.getProperty("SEARCH_BASE"), "sAMAccountName="+username, searchControls);
			if (!sr.hasMoreElements())
				throw new FailedLoginException("Login failed");
			while (sr.hasMore()) {
				SearchResult next = sr.next();
				Attributes attrs = next.getAttributes();
				Attribute attr = attrs.get("memberOf");
				if (attr == null)
					throw new FailedLoginException("Login failed");				
				if (!attr.contains(prop.getProperty("MEMBER_OF")))
					throw new FailedLoginException("Login failed");
			}
			return true;
		} catch (NamingException e) {
			throw new FailedLoginException("Login failed");
		}
	}
}
