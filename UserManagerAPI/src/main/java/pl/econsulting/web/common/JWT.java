package pl.econsulting.web.common;

import java.util.Date;

import javax.security.auth.login.FailedLoginException;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import io.jsonwebtoken.*;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.impl.DefaultClaims;
import pl.econsulting.web.common.config.AppProperties;

public class JWT {
	
	private static SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;
	
	public static String createJWT(JSONObject JWTBody ) {
		AppProperties prop = AppProperties.getAppProperties();
		Claims claims = new DefaultClaims();
		claims.setId(JWTBody.getString("id"));
		claims.setIssuer(JWTBody.getString("issuer"));
		claims.setSubject(JWTBody.getString("subject"));
		claims.put("jobRole", JWTBody.get("jobRole"));
		claims.setIssuedAt(new Date());
		JwtBuilder jwtBuilder = Jwts.builder().setClaims(claims).signWith(SIGNATURE_ALGORITHM, prop.getProperty("TOKEN_PASSWORD")).setHeaderParam("type","JWT");
		
		return jwtBuilder.compact();
	}
	
	public static JSONObject verifyJWT(HttpServletRequest request) throws FailedLoginException {
		AppProperties prop = AppProperties.getAppProperties();
		String authHeader = request.getHeader("Authorization");
		if (authHeader == null)
			throw new FailedLoginException("no authorization header privded");
		if (!authHeader.contains("Bearer "))
			throw new FailedLoginException("you should use Bearer type of auth");
		String token = authHeader.substring("Bearer ".length(), authHeader.length());
		Claims claims = null;
		try {
			claims = Jwts.parser().setSigningKey(prop.getProperty("TOKEN_PASSWORD")).parseClaimsJws(token).getBody();
		} catch (ExpiredJwtException | UnsupportedJwtException | 
				MalformedJwtException | SignatureException | IllegalArgumentException e) {
			throw new FailedLoginException("invalid token");
		}
		JSONObject JWTBody = new JSONObject();
		JWTBody.put("id", claims.getId());
		JWTBody.put("issuer", claims.getIssuer());
		JWTBody.put("issuedAt", claims.getIssuedAt());
		JWTBody.put("subject", claims.getSubject());
		if (claims.get("jobRole") == null) 
			throw new JwtException("Malformed payload");
		else {
			 JWTBody.put("jobRole", claims.get("jobRole"));
			 if (claims.getIssuedAt().getTime()+Long.valueOf(prop.getProperty("TOKEN_TTL")) < System.currentTimeMillis())
				 throw new JwtException("outdated token");
		 }
		return JWTBody;
	}	
}
