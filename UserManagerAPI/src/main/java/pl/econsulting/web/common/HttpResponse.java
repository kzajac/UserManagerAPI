package pl.econsulting.web.common;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

public class HttpResponse {
	
	public static void SendResponse(HttpServletResponse response, JSONObject respBody, int sc_code) {
		response.setStatus(sc_code);
		response.setHeader("Content-Type", "application/json;");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Access-Control-Allow-Origin","*");
		PrintWriter out;
		try {
			out = response.getWriter();
			out.write(respBody.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void SendResponse(HttpServletResponse response, JSONArray respArray, int sc_code) {
		response.setStatus(sc_code);
		response.setHeader("Content-Type", "application/json;");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Access-Control-Allow-Origin","*");
		PrintWriter out;
		try {
			out = response.getWriter();
			out.write(respArray.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
