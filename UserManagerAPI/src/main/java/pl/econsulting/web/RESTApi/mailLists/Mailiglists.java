package pl.econsulting.web.RESTApi.mailLists;

import java.util.ArrayList;
import java.util.List;
import javax.security.auth.login.FailedLoginException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.jsonwebtoken.JwtException;
import pl.econsulting.dao.DAOException;
import pl.econsulting.dao.mailingList.MailingListDAOFactory;
import pl.econsulting.model.MailingList;
import pl.econsulting.web.common.JWT;

@Path("/mailinglists/mailinglist")
public class Mailiglists {
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response addNewMailinglist(@Context HttpServletRequest request, String requestBody) {
		try {
			JWT.verifyJWT(request);
		} catch (JwtException | FailedLoginException e) {
			//...if no send 401 unauthorized
			JSONObject respBody = new JSONObject();
			respBody.put("error", "request_unauthorized");
			respBody.put("error_description", "malformed or outdated token");
			return Response.status(401).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		}
		JSONObject requestJson;
		String listName, listEmailAddress;
		List<String> gotoList = new ArrayList<String>();
		try {
			requestJson = new JSONObject(requestBody);
			listName = requestJson.getString("listName");
			listEmailAddress = requestJson.getString("emailAddress");
			JSONArray tempGoto = requestJson.getJSONArray("gotoList");
			if (tempGoto != null) {
				for (int i=0;i<tempGoto.length();i++)
					gotoList.add(tempGoto.get(i).toString());
			}
		} catch (JSONException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "malformed_payload");
			respBody.put("error_description", e.getMessage());
			return Response.status(400).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		}
		if ((listName == null) || (listEmailAddress == null) || (gotoList.isEmpty())) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "malformed_payload");
			respBody.put("error_description", "not all parameters were given!");
			return Response.status(400).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		}
		MailingList mailinglist = new MailingList();
		mailinglist.setName(listName);
		mailinglist.setMailAddress(listEmailAddress);
		mailinglist.setAddressList(gotoList);
		try {
			MailingListDAOFactory.getMailingListDAOFactory().createMailingList(mailinglist);
			JSONObject respBody = new JSONObject();
			respBody.put("OK", "New Mailinglist has been successfully added!");
			return Response.status(200).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		} catch (DAOException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "Database_error");
			respBody.put("error_description", e.getMessage());
			return Response.status(500).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllMailingLists(@Context HttpServletRequest request) {
		try {
			JWT.verifyJWT(request);
			List<MailingList> list = MailingListDAOFactory.getMailingListDAOFactory().readAllMailingLists();
			JSONArray body = new JSONArray();
			for (MailingList tempList : list) {
				JSONObject tempJson = new JSONObject();
				tempJson.put("name", tempList.getName());
				tempJson.put("emailAddress", tempList.getMailAddress());
				//JSONArray tempArray = new JSONArray();
				tempJson.put("AddressList", tempList.getAddressList());
				tempJson.put("availableEmailAddresses", tempList.getAvailavleAddresses());
				body.put(tempJson);
			}
			return Response.status(200).header("Access-Control-Allow-Origin","*").entity(body.toString()).build();
		} catch (DAOException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "Database_error");
			respBody.put("error_description", "something went wrong on a database level");
			return Response.status(500).entity(respBody.toString()).build();
		} catch (JwtException | FailedLoginException e) {
			//...if no send 401 unauthorized
			JSONObject respBody = new JSONObject();
			respBody.put("error", "request_unauthorized");
			respBody.put("error_description", "malformed or outdated token");
			return Response.status(401).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		}
	}
	
	@OPTIONS
	public Response doOptions() {
		return Response.status(200).header("Access-Control-Allow-Origin","*").header("Access-Control-Allow-Methods", "DELETE,GET,PUT,POST,OPTIONS")
				.header("Access-Control-Allow-Headers", "X-Requested-With, content-type, Authorization").build();
	}
}