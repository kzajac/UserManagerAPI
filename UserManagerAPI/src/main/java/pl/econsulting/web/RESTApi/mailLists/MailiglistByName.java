package pl.econsulting.web.RESTApi.mailLists;

import java.util.List;
import javax.security.auth.login.FailedLoginException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONException;
import org.json.JSONObject;

import io.jsonwebtoken.JwtException;
import pl.econsulting.dao.DAOException;
import pl.econsulting.dao.mailingList.MailingListDAO;
import pl.econsulting.dao.mailingList.MailingListDAOFactory;
import pl.econsulting.model.MailingList;
import pl.econsulting.web.common.JWT;

@Path("/mailinglists/mailinglist/{mailinglistName}")
public class MailiglistByName {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMailingList(@Context HttpServletRequest request, @PathParam("mailinglistName") String name) {
		try {
			JWT.verifyJWT(request);
			MailingList list = MailingListDAOFactory.getMailingListDAOFactory().readMailingList(name);
			JSONObject body = new JSONObject();
			body.put("name", list.getName());
			body.put("emailAddress", list.getMailAddress());
			body.put("AddressList", list.getAddressList());
			body.put("availableEmailAddresses", list.getAvailavleAddresses());
			return Response.status(200).header("Access-Control-Allow-Origin","*").entity(body.toString()).build();
		} catch (DAOException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "Database_error");
			respBody.put("error_description", e.getMessage());
			return Response.status(500).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		} catch (JwtException | FailedLoginException e) {
			//...if no send 401 unauthorized
			JSONObject respBody = new JSONObject();
			respBody.put("error", "request_unauthorized");
			respBody.put("error_description", "malformed or outdated token");
			return Response.status(401).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		}
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addEmailToList(@Context HttpServletRequest request, String PostedData) {
		try {
			JWT.verifyJWT(request);
			JSONObject body = new JSONObject(PostedData);
			String action = body.getString("action");
			String listName = body.getString("name");
			String email = body.getString("address");
			if ((listName == null) || (email == null) || (action == null)) 
				throw new JSONException("wrong parameters");
			MailingListDAO df = MailingListDAOFactory.getMailingListDAOFactory();
			MailingList list = df.readMailingList(listName);
			if (action.equalsIgnoreCase("add")) {
				if (!list.getAvailavleAddresses().contains(email)) {
					JSONObject respBody = new JSONObject();
					respBody.put("error", "wrong_email");
					respBody.put("error_description", "given email address is not present on available emails list!");
					return Response.status(400).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
				}
				List<String> tempList = list.getAddressList();
				tempList.add(email);
				list.setAddressList(tempList);
				df.updateMailingList(list);
				JSONObject respBody = new JSONObject();
				respBody.put("OK", "Address has been successfully added!");
				return Response.status(200).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
			} else if (action.equals("remove")) {
				if (!list.getAddressList().contains(email)) {
					JSONObject respBody = new JSONObject();
					respBody.put("error", "wrong_email");
					respBody.put("error_description", "given email address is not present on email list!");
					return Response.status(400).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
				}
				List<String> tempList = list.getAddressList();
				tempList.remove(email);
				list.setAddressList(tempList);
				df.updateMailingList(list);
				JSONObject respBody = new JSONObject();
				respBody.put("OK", "Address has been successfully removed!");
				return Response.status(200).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
			} else 
				throw new JSONException("wrong action!");
		} catch (JSONException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "malformed_payload");
			respBody.put("error_description", e.getMessage());
			return Response.status(400).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		} catch(DAOException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "Database_error");
			respBody.put("error_description", e.getMessage());
			return Response.status(500).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		} catch (JwtException | FailedLoginException e) {
			//...if no send 401 unauthorized
			JSONObject respBody = new JSONObject();
			respBody.put("error", "request_unauthorized");
			respBody.put("error_description", "malformed or outdated token");
			return Response.status(401).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		}
	}
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteMailinglistByName(@Context HttpServletRequest request, @PathParam("mailinglistName") String name) {
		try {
			//check if a proper Bearer was sent
			JWT.verifyJWT(request);
			
			MailingListDAO df = MailingListDAOFactory.getMailingListDAOFactory();
			MailingList list = df.readMailingListByAddress(name);
			df.deleteMailingList(list);
			JSONObject respBody = new JSONObject();
			respBody.put("OK", "command_successful");
			respBody.put("OK_desciption", "mailinglist has been removed");
			return Response.status(200).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		} catch (DAOException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "Database_error");
			respBody.put("error_description", e.getMessage());
			return Response.status(500).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();	
		} catch (JwtException | FailedLoginException e) {
			//...if no send 401 unauthorized
			JSONObject respBody = new JSONObject();
			respBody.put("error", "request_unauthorized");
			respBody.put("error_description", "malformed or outdated token");
			return Response.status(401).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		}
	}
	
	@OPTIONS
	public Response doOptions() {
		return Response.status(200).header("Access-Control-Allow-Origin","*").header("Access-Control-Allow-Methods", "DELETE, GET, PUT, OPTIONS")
				.header("Access-Control-Allow-Headers", "X-Requested-With, content-type, Authorization").build();
	}
}
