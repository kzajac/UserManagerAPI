package pl.econsulting.web.RESTApi;

import static pl.econsulting.web.common.HttpResponse.SendResponse;
import java.io.IOException;
import java.util.Base64;
import java.util.UUID;

import javax.security.auth.login.FailedLoginException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import pl.econsulting.web.common.JWT;
import pl.econsulting.web.common.LdapAuthenticate;
import pl.econsulting.web.common.config.AppProperties;


@WebServlet("/api/v1/signin")
public class SignIn extends HttpServlet {

	private static final long serialVersionUID = 4277651621129518015L;
	private static final String issuer = "UserManagerAPI";
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		JSONObject respBody = new JSONObject();
		String authHeader = request.getHeader("Authorization");
		if (authHeader == null) {
			//request doesn't contain Authorization header
			respBody.put("error", "invalid_client");
			respBody.put("error_description", "No HttpAuth provied");
			SendResponse(response, respBody, HttpServletResponse.SC_UNAUTHORIZED);
		} else {
			if (authHeader.contains("Basic ")) {
				String authHeaderToDecode = authHeader.substring("Basic ".length(), authHeader.length());
				try {
					String usernpass = new String(Base64.getDecoder().decode(authHeaderToDecode));
					String username = usernpass.substring(0,usernpass.indexOf(":")).replace("[^A-Za-z0-9]", "");
					String password = usernpass.substring(usernpass.indexOf(":")+1).replace("[^A-Za-z0-9]", "");
					if (LdapAuthenticate.authenticate(username, password)) {
						String grantType = request.getParameter("grant_type");
						if (grantType == null) {
							//grant-type parameter is null
							respBody.put("error", "unsupported_grant_type");
							respBody.put("error_description", "Grant type is NULL");
							SendResponse(response, respBody, HttpServletResponse.SC_BAD_REQUEST);
						} else {
							if (grantType.equals("client_credentials")) {
								//grant type is present and has a proper value, let's create a token
								JSONObject token = generateToken(username);
								SendResponse(response, token, HttpServletResponse.SC_OK);
							} else {
								//grant-type is present, but is has wrong value
								respBody.put("error", "unsupported_grant_type");
								respBody.put("error_description", "unsupported grant_type");
								SendResponse(response, respBody, HttpServletResponse.SC_BAD_REQUEST);
							}
						}
					}
				} catch (IllegalArgumentException | FailedLoginException e) {
					//request's auth header wasn't base64 encoded or authentication failed
					respBody.put("error", "invalid_client");
					respBody.put("error_description", e.getMessage());
					SendResponse(response, respBody, HttpServletResponse.SC_UNAUTHORIZED);
				} 
			} else {
				//request doesn't have a BASIC authentication header
				respBody.put("error", "invalid_client");
				respBody.put("error_description", "Client Authentication failed");
				SendResponse(response, respBody, HttpServletResponse.SC_UNAUTHORIZED);
			}
		}
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		doPost(request,response);
	}
	
	public void doOptions(HttpServletRequest request, HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin","*");
		response.addHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
		response.addHeader("Access-Control-Allow-Credentials", "false");
		response.addHeader("Access-Control-Max-Age", "86400");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, authorization");
	}
	
	private JSONObject generateToken(String subject) {
		AppProperties prop = AppProperties.getAppProperties();
		JSONObject JWTBody = new JSONObject();
		JWTBody.put("id", UUID.randomUUID().toString());
		JWTBody.put("issuer", issuer);
		JWTBody.put("subject", subject);
		JWTBody.put("jobRole", "admin");
		JSONObject JSONToken = new JSONObject();
		JSONToken.put("access_token", JWT.createJWT(JWTBody));
		JSONToken.put("expires_in", prop.getProperty("TOKEN_TTL"));
		return JSONToken;
	}
}
