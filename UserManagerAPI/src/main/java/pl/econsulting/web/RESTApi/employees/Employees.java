package pl.econsulting.web.RESTApi.employees;

import static pl.econsulting.web.common.HttpResponse.SendResponse;

import java.io.IOException;
import java.util.List;
import javax.security.auth.login.FailedLoginException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import io.jsonwebtoken.JwtException;
import pl.econsulting.dao.DAOException;
import pl.econsulting.dao.user.EmployDAOFactory;
import pl.econsulting.model.Employ;
import pl.econsulting.web.common.GetJSON;
import pl.econsulting.web.common.JWT;

@WebServlet("/api/v1/employees/employ")
public class Employees extends HttpServlet {

	private static final long serialVersionUID = -3770395217682346507L;

	/**
	 * adds a new employ
	 * @throws IOException 
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		try {
			//check if a proper Bearer was sent
			JWT.verifyJWT(request);
			response.addHeader("Access-Control-Allow-Origin","*");
			JSONObject newEmploy = GetJSON.getJson(request);
			String name = newEmploy.getString("name");
			String surname = newEmploy.getString("surname");
			String email = newEmploy.getString("email");
			String username = newEmploy.getString("username");
			String password = newEmploy.getString("password");
			String userRole = newEmploy.getString("userRole");
			if ((email == null) || (name == null) || (surname == null) || (username == null)
					|| (password == null) || (userRole == null))
				throw new JSONException("wrong parameters");
			Employ user = new Employ();
			user.setName(name);
			user.setSurname(surname);
			user.setEmail(email);
			user.setUsername(username);
			user.setPassword(password);
			user.setUserRole(userRole);
			EmployDAOFactory.getEmployDAO().createEmploy(user);
			JSONObject respBody = new JSONObject();
			respBody.put("OK", "New employ has been successfully added!");
			SendResponse(response, respBody, HttpServletResponse.SC_OK);
		} catch (JwtException | FailedLoginException e) {
			//...if no send 401 unauthorized
			JSONObject respBody = new JSONObject();
			respBody.put("error", "request_unauthorized");
			respBody.put("error_description", "malformed or outdated token");
			SendResponse(response, respBody, HttpServletResponse.SC_UNAUTHORIZED);
			return;
		} catch (IOException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "Server_error");
			respBody.put("error_description", "IOExcetpion");
			SendResponse(response, respBody, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			//sent body is not in JSON format...
			JSONObject respBody = new JSONObject();
			respBody.put("error", "malformed_payload");
			respBody.put("error_description", "unable to parse JSON");
			SendResponse(response, respBody, HttpServletResponse.SC_BAD_REQUEST);
		} catch (DAOException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "Database_error");
			respBody.put("error_description", e.getMessage());
			SendResponse(response, respBody, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * list all employees
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		try {
			//check if a proper Bearer was sent
			JWT.verifyJWT(request);
			List<Employ> employeesList = EmployDAOFactory.getEmployDAO().findAll();
			JSONArray body = new JSONArray();
			for (Employ user : employeesList) {
				JSONObject currentUser = new JSONObject();
				currentUser.put("name", user.getName());
				currentUser.put("surname", user.getSurname());
				currentUser.put("username", user.getUsername());
				currentUser.put("email", user.getEmail());
				currentUser.put("userrole", user.getUserRole());
				body.put(currentUser);
			}
			response.addHeader("Access-Control-Allow-Origin","*");
			SendResponse(response, body, HttpServletResponse.SC_OK);
		} catch (JwtException | FailedLoginException e) {
			//...if no send 401 unauthorized
			JSONObject respBody = new JSONObject();
			respBody.put("error", "request_unauthorized");
			respBody.put("error_description", "malformed or outdated token");
			SendResponse(response, respBody, HttpServletResponse.SC_UNAUTHORIZED);
			return;
		} catch (DAOException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "Database_error");
			respBody.put("error_description", "something went wrong on a database level");
			SendResponse(response, respBody, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
	
	public void doOptions(HttpServletRequest request, HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin","*");
		response.addHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
		response.addHeader("Access-Control-Allow-Credentials", "false");
		response.addHeader("Access-Control-Max-Age", "86400");
		response.addHeader("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization");
	}
}
