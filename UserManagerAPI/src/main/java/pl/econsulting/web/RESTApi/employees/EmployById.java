package pl.econsulting.web.RESTApi.employees;

import java.util.List;

import javax.security.auth.login.FailedLoginException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;
import io.jsonwebtoken.JwtException;
import pl.econsulting.dao.DAOException;
import pl.econsulting.dao.mailingList.MailingListDAO;
import pl.econsulting.dao.mailingList.MailingListDAOFactory;
import pl.econsulting.dao.user.EmployDAO;
import pl.econsulting.dao.user.EmployDAOFactory;
import pl.econsulting.model.Employ;
import pl.econsulting.model.MailingList;
import pl.econsulting.web.common.JWT;

@Path("/employees/employ/{employId}")
public class EmployById extends Application {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEmployById(@Context HttpServletRequest request, @PathParam("employId") String username) {
		try {
			//check if a proper Bearer was sent
			JWT.verifyJWT(request);
			
			Employ employ = EmployDAOFactory.getEmployDAO().readEmploy(username);
			JSONObject employJson = new JSONObject();
			employJson.put("name", employ.getName());
			employJson.put("surname", employ.getSurname());
			employJson.put("email", employ.getEmail());
			employJson.put("username", employ.getUsername());
			employJson.put("onmailinglists", employ.getAddedToLists());
			return Response.status(200).header("Access-Control-Allow-Origin","*").entity(employJson.toString()).build();
		} catch (JwtException | FailedLoginException e) {
			//...if no send 401 unauthorized
			JSONObject respBody = new JSONObject();
			respBody.put("error", "request_unauthorized");
			respBody.put("error_description", "malformed or outdated token");
			return Response.status(401).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		} catch (DAOException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "Database_error");
			respBody.put("error_description", e.getMessage());
			return Response.status(500).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		}
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateEmploy(@Context HttpServletRequest request, @PathParam("employId") String username, String postedData) {
		try {
			//check if a proper Bearer was sent
			JWT.verifyJWT(request);
			//checks if an employ doesn't exist already
			EmployDAO df = EmployDAOFactory.getEmployDAO();
			Employ employ = df.readEmploy(username);
			
			JSONObject requestBody = new JSONObject(postedData);
			String name = requestBody.getString("name");
			String surname = requestBody.getString("surname");
			String email = requestBody.getString("email");
			if ((name == null) || (surname == null) || (email == null)) 
				throw new JSONException("wrong parameters");
			employ.setUsername(username);
			employ.setName(name);
			employ.setSurname(surname);
			employ.setEmail(email);
			df.updateEmploy(employ);
			JSONObject respBody = new JSONObject();
			respBody.put("OK", "Employ has been successfully updated!");
			return Response.status(200).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		} catch (JSONException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "malformed_payload");
			respBody.put("error_description", e.getMessage());
			return Response.status(400).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		} catch (DAOException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "Database_error");
			respBody.put("error_description", e.getMessage());
			return Response.status(500).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		}  catch (JwtException | FailedLoginException e) {
			//...if no send 401 unauthorized
			JSONObject respBody = new JSONObject();
			respBody.put("error", "request_unauthorized");
			respBody.put("error_description", "malformed or outdated token");
			return Response.status(401).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		}
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteEmployById(@Context HttpServletRequest request, @PathParam("employId") String username) {
		try {
			//check if a proper Bearer was sent
			JWT.verifyJWT(request);
			
			EmployDAO df = EmployDAOFactory.getEmployDAO();
			Employ employ = df.readEmploy(username);
			List<String> onMailingLists = employ.getAddedToLists();
			MailingListDAO mdf = MailingListDAOFactory.getMailingListDAOFactory();
			if (onMailingLists != null) {
				for (String mailingList : onMailingLists) {
					MailingList list = mdf.readMailingList(mailingList);
					List<String> tempList = list.getAddressList();
					tempList.remove(employ.getEmail());
					list.setAddressList(tempList);
					mdf.updateMailingList(list);
				}
			}
			df.deleteEmploy(employ);
			JSONObject respBody = new JSONObject();
			respBody.put("OK", "command_successful");
			respBody.put("OK_desciption", "employ has been removed");
			return Response.status(200).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		} catch (DAOException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "Database_error");
			respBody.put("error_description", e.getMessage());
			return Response.status(500).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		} catch (JwtException | FailedLoginException e) {
			//...if no send 401 unauthorized
			JSONObject respBody = new JSONObject();
			respBody.put("error", "request_unauthorized");
			respBody.put("error_description", "malformed or outdated token");
			return Response.status(401).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		}
	}
	
	@OPTIONS
	public Response doOptions(@PathParam("employId") String id) {
		return Response.status(200).header("Access-Control-Allow-Origin","*").header("Access-Control-Allow-Methods", "DELETE,GET,PUT,OPTIONS")
				.header("Access-Control-Allow-Headers", "X-Requested-With, content-type, Authorization").build();
	}
}