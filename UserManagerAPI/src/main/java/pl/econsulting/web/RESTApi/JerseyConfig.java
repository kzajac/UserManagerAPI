package pl.econsulting.web.RESTApi;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

//@ApplicationPath("api/v1/employees/employ")
@ApplicationPath("api/v1")
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        packages("pl.econsulting.web.RESTApi");
    }
}