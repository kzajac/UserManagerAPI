package pl.econsulting.web.RESTApi.mailLists;

import java.util.List;

import javax.security.auth.login.FailedLoginException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import io.jsonwebtoken.JwtException;
import pl.econsulting.dao.DAOException;
import pl.econsulting.dao.mailingList.MailingListDAOFactory;
import pl.econsulting.web.common.JWT;

@Path("/mailinglists/emailaddresses")
public class EmailAddresses {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllemailAddresses(@Context HttpServletRequest request) {
		try {
			JWT.verifyJWT(request);
			List<String> temp = MailingListDAOFactory.getMailingListDAOFactory().readAllEmailAddresses();
			JSONArray toReturn = new JSONArray();
			for (String address : temp)
				toReturn.put(address);
			return Response.status(200).header("Access-Control-Allow-Origin","*").entity(toReturn.toString()).build();
		} catch (DAOException e) {
			JSONObject respBody = new JSONObject();
			respBody.put("error", "database_error");
			respBody.put("error_description", e.getMessage());
			return Response.status(400).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		} catch (JwtException | FailedLoginException e) {
			//...if no send 401 unauthorized
			JSONObject respBody = new JSONObject();
			respBody.put("error", "request_unauthorized");
			respBody.put("error_description", "malformed or outdated token");
			return Response.status(400).header("Access-Control-Allow-Origin","*").entity(respBody.toString()).build();
		}
	}
	
	@OPTIONS
	public Response doOptions() {
		return Response.status(200).header("Access-Control-Allow-Origin","*").header("Access-Control-Allow-Methods", "DELETE, GET, PUT, OPTIONS")
				.header("Access-Control-Allow-Headers", "X-Requested-With, content-type, Authorization").build();
	}
}
