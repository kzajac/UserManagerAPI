package pl.econsulting.common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.*;
import javax.sql.DataSource;
import java.security.SecureRandom;

public class UserAuthentication {
	
	public static final String SECURE_ALGORITHM = "SHA-1";
	public static final String RANDOM_NUMBER_GENERATOR = "SHA1PRNG";
	public static final int ITERATION_NUMBER = 1000;
	
	private static final String SELECT_USER_QUERY = "Select Login from Users where Login=?";
	private static final String UPDATE_USER_QUERY = "Update Users set Password=?, Salt=? where Login=?";
	private static final String INSERT_USER_QUERY = "Insert into Users(Login, Password, Salt, Active) values(?,?,?,?)";
	private static final String AUTH_SELECT_USER_QUERY = 
			"SELECT PASSWORD, SALT FROM USERS WHERE LOGIN = ? AND ACTIVE=?";
	private static final String CREATE_AUTH_TABLE_QUERY = 
			"CREATE TABLE Users (UserID int auto_increment PRIMARY KEY, Login VARCHAR(32) UNIQUE NOT NULL, Password VARCHAR(32) NOT NULL, Salt VARCHAR(32) NOT NULL, Active bool NOT NULL);";
	
	public static boolean authenticate(String login, String password) {
		boolean userExist = true;
        // INPUT VALIDATION
		if (login==null||password==null) {
    		// TIME RESISTANT ATTACK
            // Computation time is equal to the time needed by a legitimate user
            userExist = false;
            login="";
            password="";
		}
		DataSource ds = MySQLConnectionPoolFactory.getDataSource();
		try (Connection con = ds.getConnection();
				PreparedStatement ps = con.prepareStatement(AUTH_SELECT_USER_QUERY);) {
			ps.setString(1, login);
    	    ps.setBoolean(2, true);
    	    ResultSet rs = ps.executeQuery();
    	    String digest, salt;
    	    if (rs.next()) {
    	    	digest = rs.getString("PASSWORD");
    	    	salt = rs.getString("SALT");
    	    	if (digest == null || salt == null)
    	    		//throw new SQLException("Database inconsistant Salt or Digested Password altered");
    	    		return false;
    	    	if (rs.next()) 
    	    		//throw new SQLException("Database inconsistent two CREDENTIALS with the same LOGIN");
    	    		return false;
    	    } else {
    	    	digest = "000000000000000000000000000=";
    	        salt = "00000000000=";
    	        userExist = false;
    	    }
    	    if (con != null)
    	    	con.close();
    	    byte[] bDigest = base64ToByte(digest);
	           byte[] bSalt = base64ToByte(salt);
	 
	           // Compute the new DIGEST
	           byte[] proposedDigest = getHash(password, bSalt);
	 
	           return Arrays.equals(proposedDigest, bDigest) && userExist;
		} catch(SQLException | IOException | NoSuchAlgorithmException e) {
			return false;
		}
	}
	
	/**
	 * Digests user's password provided as clear text and stores it in the database
	 * 
	 * @param con The database connection
	 * @param login The user's login
	 * @param password The user's password as clear text
	 * @return TRUE if user was successfully added to database, FALSE otherwise
	 * @throws SQLException
	 * @throws NoSuchAlgorithmException
	 */
	@SuppressWarnings("resource")
	public static boolean addUser(String login, String password) throws SQLException, NoSuchAlgorithmException {
		if (login != null && password != null) {
			DataSource
			ds = MySQLConnectionPoolFactory.getDataSource();
			SecureRandom random = SecureRandom.getInstance(RANDOM_NUMBER_GENERATOR);
			//create a salt
			byte[] bsalt = new byte[8];
			random.nextBytes(bsalt);
			byte[] bDigest = getHash(password, bsalt);
			String sDigest = byteToBase64(bDigest);
			String sSalt = byteToBase64(bsalt);
			
			//store a new user in a database
			Connection con = ds.getConnection();
			PreparedStatement pstmt = con.prepareStatement(SELECT_USER_QUERY);
			pstmt.setString(1, login);
			ResultSet rs = pstmt.executeQuery();
			pstmt.clearBatch();
			if (rs.isBeforeFirst()) {
				//it means that user already exists in a database
				pstmt = con.prepareStatement(UPDATE_USER_QUERY);
				pstmt.setString(1, sDigest);
				pstmt.setString(2, sSalt);
				pstmt.setString(3, login);
				pstmt.execute();
				pstmt.clearBatch();
			} else {
				//User needs to be added to a database
				pstmt = con.prepareStatement(INSERT_USER_QUERY);
				pstmt.setString(1, login);
				pstmt.setString(2, sDigest);
				pstmt.setString(3, sSalt);
				pstmt.setBoolean(4, true);
				pstmt.execute();
			}
			if (pstmt != null)
				pstmt.close();
			con.close();
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * creates a table for username entries
	 * 
	 * @param con a database connection
	 * @return TRUE if a table was successfully created, or FALSE otherwise
	 */
	public static boolean createTable(Connection con) {
		try (Statement stmt = con.createStatement()) {
			stmt.execute(CREATE_AUTH_TABLE_QUERY);
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/**
	 * Returns a digested password based on password as a clear text and a salt
	 * 
	 * @param password The password which should be encrypted
	 * @param salt The salt
	 * @return byte[] The digested password
	 * @throws NoSuchAlgorithmException
	 */
	public static byte[] getHash(String password, byte[] salt) throws NoSuchAlgorithmException {
		MessageDigest  digest = MessageDigest.getInstance(SECURE_ALGORITHM);
		digest.reset();
		digest.update(salt);
		byte[] output = null;
		try {
			output = digest.digest(password.getBytes("UTF-8"));
			for (int i = 0; i < ITERATION_NUMBER; i++) {
				digest.reset();
				output = digest.digest(output);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return  output;
	}
	
	public static byte[] base64ToByte(String data) throws IOException{
		//BASE64Decoder decoder = new BASE64Decoder();
		Decoder decoder = Base64.getDecoder();
		return decoder.decode(data);
	}
	
	public static String byteToBase64(byte[] data) {
		//BASE64Encoder encoder = new BASE64Encoder();
		Encoder encoder = Base64.getEncoder();
		return encoder.encodeToString(data);
	}
}
