package pl.econsulting.common;


import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class MySQLConnectionPoolFactory {
	
	private static final String SQL_DATASOURCE = "java:/comp/env/jdbc/ecUsermanagerDB";
	private static DataSource ds=null;
	private MySQLConnectionPoolFactory() {}
	
	public static final synchronized DataSource getDataSource() {
		if (ds == null) {
			try {
				InitialContext icx = new InitialContext();
				ds = (DataSource)icx.lookup(SQL_DATASOURCE);
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		return ds;
	}
}

