package pl.econsulting.common;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;

import pl.econsulting.web.common.config.AppProperties;

public class LDAPConnectionFactory {
	
	public static InitialLdapContext getLDAPConnection() throws NamingException {
		AppProperties prop = AppProperties.getAppProperties();
		Hashtable<String, String> env = new Hashtable<String, String>();
		
		//set auth method, username and pass
		env.put(Context.INITIAL_CONTEXT_FACTORY, prop.getProperty("INITIAL_CONTEXT_FACTORY"));
		env.put(Context.SECURITY_AUTHENTICATION, prop.getProperty("SECURITY_AUTHENTICATION"));
		env.put(Context.SECURITY_PRINCIPAL, prop.getProperty("ADMIN_NAME"));
		env.put(Context.SECURITY_CREDENTIALS, prop.getProperty("ADMIN_PASS"));
		
		env.put(Context.PROVIDER_URL, prop.getProperty("PROVIDER_URL"));
		return new InitialLdapContext(env, null);
	}
}
